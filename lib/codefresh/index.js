module.exports = {
  apiVersion: 1,
  name: 'Codefresh',
  parse: function (headers, body) {
    return {
      message: body.message,
      icon: body.status === 'ok' ? 'logo' : 'error',
      errorLevel: body.status === 'ok' ? 'normal' : 'error',
    };
  },
};
