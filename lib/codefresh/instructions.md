### Codefresh Gitter notify plugin

You need to configure the [Gitter plugin](https://steps.codefresh.io/step/gitter) for your Codefresh pipeline.
