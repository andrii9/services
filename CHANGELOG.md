# 1.23.0 - 2018-10-31

 - Only generate notifications for completed Heroku app update events
    - Thanks to [@wlach](https://gitlab.com/wlach) for the contribution, https://gitlab.com/gitlab-org/gitter/services/merge_requests/101
